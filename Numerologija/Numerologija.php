<?php

// Za vezbu ime je dodeljeno random.
// U numerologiji svaki broj ima odredjeno znacenje. 
// Dan, mesec i godina rodjenja se pretvaraju u jednocifrene brojeve (recimo, 12 je 1+2=3)
// tako da konacan zbir tako svedenih jednocifrenih  brojeva dan mesec i godina se ponovo sabiraju dok se ne 
// dobije konacan jednocifren broj.
// izuzetak su brojevi 11 i 22 koji se ne svode.

// slican postupak je i za Ime, tj slova, pri cemu a ima vrednost 1,i-> 9, a zatim je slovu j dodeljuje ponovo 1 
// i tako u krug.

// kombinacija datuma (zivotni put) i imena (sudbinski broj) svedenih na jednocifrene brojeve daju "jedinstveni
// numericki horoskop" za pojedinca.

class Numerologija
{
	public $dan;
	public $mesec;
	public $godina;
        public static $nizSlovaZaRacunanje = array
        (
            "a"=>1,
            "b"=>2,
            "c"=>3,
            "d"=>4,
            "e"=>5,
            "f"=>6,
            "g"=>7,
            "h"=>8,
            "i"=>9            
        );
        public static $nizSlova=array('a','b','c','d','e','f','g','h','i');
	
	public function __construct ($dan,$mesec,$godina)
	{
		$this->dan=$dan;
		$this->mesec=$mesec;
		$this->godina=$godina;
	}
	
	public function GetDan()
	{
		return $this->dan;
	}
        
        public function GetMesec()
        {
            return $this->mesec;
        }
        
        public function GetGodina()
        {
            return $this->godina;
        }
	// rekurzija
	public function toOneDigit($number)
	{
		 
            if ($number==11 || $number==22)
            {  
                $sum = $number;

            }
            else
            {
               $num_array=str_split($number);
               $sum=0;


               foreach($num_array as $num)
               {
                       $sum+=$num;
               }
            }
            if(strlen($sum)>1 && $sum!=11 && $sum!=22)
            {
                   return $this->toOneDigit($sum);
            }

                   return $sum; 
	}
        
        
        public function IzracunajZivotniPut()
        {
           $dan     = $this->toOneDigit($this->GetDan());
           $mesec   = $this->toOneDigit($this->GetMesec());
           $godina  = $this->toOneDigit($this->GetGodina());
           
            $final= $dan + $mesec + $godina;
            
           return  $this->toOneDigit($final);
            
        }
        
        public function GetName ()
        {
            $randomIme='';
            
            for($i=0;$i<7;$i++)
            {
                $randomIme.=static::$nizSlova[rand(0,8)];
            }
            
            return $randomIme;
            
        }
        
        public function IzracunajSudbinskiBroj()
        {
            $ime=$this->GetName();
            $nizKaraktera= str_split($ime);
            $sum=0;
            foreach ($nizKaraktera as $slovo)
            {
                $sum+=static::$nizSlovaZaRacunanje[$slovo];
            }
            $sb=$this->toOneDigit($sum);
            
            $nizAsoc=array($ime=>$sb);
            return $nizAsoc;
        }

}

if (isset($_POST['submit']))
{
    
    echo $_POST['dan'].'.'.$_POST['mesec'].'.'.$_POST['godina']."<br>";
    $num= new Numerologija ($_POST['dan'],$_POST['mesec'],$_POST['godina']);
    //echo $num->IzracunajZivotniPut();
    $nsb=$num->IzracunajSudbinskiBroj();
    //echo $num->GetName();
    echo "Vase ime je: ".key($nsb)."<br>";
    echo "Vas zivotni put je: ".$num->IzracunajZivotniPut()."<br>"; 
    foreach ($nsb as $k=>$v)
    {

        echo "Sudbinski Broj: ".$v;
    }
}
?>

<br><a href='forma.php'>forma</a>