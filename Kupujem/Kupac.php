<?php


class Kupac extends Covek 
{
    public $korpa;
    public $budzet=1500;
    //public $NizProizvoda=array();
    
    public function __construct($ime, $prez, $mb,$korpa)
    {
        parent::__construct($ime, $prez, $mb);
        $this->korpa=$korpa;
    }
    
    public function renderLicniPodaci()
    {
        $licniPod = $this->ime."<br>";
        $licniPod.= $this->prezime."<br>";
        $licniPod.= $this->maticni_br."<br>";
        
        return $licniPod;
    }
    
    public function renderKorpa()
    {
        $korpa=  $this->korpa->id_korpe."<br>";
        //$korpa.= $this->korpa->nizProizvoda[0]->naziv."<br>";
        foreach ($this->korpa->nizProizvoda as $kor)
        {
            $korpa.= "Proizvod: ".$kor->naziv." , Cena : ".$kor->cena."<br>";
        }
        return $korpa;
    }
    
}
