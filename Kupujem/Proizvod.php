<?php


class Proizvod
{
   public static $brojInstanci=0;
   public $naziv;
   public $cena;
   public $nizSlova=array('a','b','c','d','e','f','g','h','i');
   
   public function __construct()
   {
       $this->naziv = $this->GetName();
       $this->cena = rand(10,2000);
   }
   
    public function GetName ()
        {
            $randomIme='';
            
            for($i=0;$i<7;$i++)
            {
                $randomIme.=$this->nizSlova[rand(0,8)];
            }
            
            return $randomIme;
            
        }
        
        
   
   
}
