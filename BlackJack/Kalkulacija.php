<?php

//Princip za kalkulaciju i odredjivanje pobednika:
//Svi igraci se sortiraju po zbiru uzevsi u obzir samo one koji imaju 21 ili manje
// ukoliko vise igraca ima isti zbir - > draw vracaju se ulozi
// medjutim, ukoliko je dealer draw, njegova je ruka i on nosi sav ulog;
class Kalkulacija 
{    
    public static function IzracunajRuku($igrac,$kompjuteri)
    {
        $nizIgraca=array();
        $kompjuteri[]=$igrac;
        $nizIgraca=$kompjuteri;
        // callback -> Thanks Joakime!
        usort($nizIgraca,function($first,$second)
        {
            return $first->Zbir() < $second->Zbir();
        });
        $nizIgracaKonacni=array();
        $nizIgraca22=[];#igraci koji su presli 21
        $imaPobednika=false;
        foreach ($nizIgraca as $ni)
        {
               if ($ni->Zbir() <=21)
               {
                    $nizIgracaKonacni[]=$ni;
                    $imaPobednika=true;
               }
               else
               {
                    $nizIgraca22[]=$ni;
               }
        }
      // var_dump($nizIgracaKonacni);
        $pobednik=null;
        if(count($nizIgracaKonacni)>=2)
        {
            if ( $nizIgracaKonacni[0]->Zbir()==$nizIgracaKonacni[1]->Zbir())
            {
                foreach ($nizIgraca as $niz)
                {
                    if ($niz->is_dealer)
                    {
                        $pobednik=$niz;
                        $pobednik->is_draw=true;
                    }
                }
            }
            else
            {
                $pobednik=$nizIgracaKonacni[0];
                $pobednik->is_draw=false;
            }
        }
        if(sizeof($nizIgracaKonacni)==1)
        {
            $pobednik=$nizIgracaKonacni[0];
            $pobednik->is_draw=false;
        }
        if($pobednik!==null)
        {
            $idPobednik=$pobednik->id;
            $zbirOstalihUloga=0;
            foreach ($nizIgraca as $nig)
            {
                if ($nig->id !=$idPobednik)
                {
                    $zbirOstalihUloga+=$nig->ulog;
                }
            }
            $pobednik->balans+=$zbirOstalihUloga;
            $pobednik->balans+=$pobednik->ulog;
        }
        if (!$imaPobednika)
        {
            foreach ($nizIgraca22 as $ni22)
            {
                $ni22->balans+=$ni22->ulog;
            }
        }
        return $pobednik;
    }
}
