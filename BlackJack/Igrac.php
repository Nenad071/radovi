<?php


abstract class Igrac {
    public $ime;
    public $prezime;
    public $ulog=0;//iznos koji uplacuje u krugu
    public $balans=0;// ukupan skor
    public $is_dealer=false;
    public $podeljeneKarte=[];
    public $id;

    public $lastStep="Hit";

    private function ZbirBezKeca()
    {
        $konacniNiz=[];
        $brojKeceva=0;
        $suma=0;
        foreach($this->podeljeneKarte as $karta)
        {
            if($karta->vrednost!=1)
            {
                 if($karta->stih)
                 {
                     $suma+=10;
                 }
                 else
                 {
                     $suma+=$karta->vrednost;
                 }
            }
            else 
            {
                $brojKeceva++;
            }
        }
        $konacniNiz['brojKeceva']=$brojKeceva;
        $konacniNiz['zbirBezKeceva']=$suma;
        return $konacniNiz;
    }
    private function VrednostKeceva()
    {
        $nizZaZbir=$this->ZbirBezKeca();
        $vrednostKeca=0;
        if ($nizZaZbir['zbirBezKeceva'] <11 && $nizZaZbir['brojKeceva']==1)
        {
            $vrednostKeca=11;
        }
        elseif ($nizZaZbir['brojKeceva']>1)
        {
             $koliko=0;
             for($i=0; $i<$nizZaZbir['brojKeceva'];$i++)
             {
                 if ($i==0)
                 {
                     $koliko=11;
                 }
                 else $koliko=1;
                 $vrednostKeca+=$koliko;
             }
        }
        elseif ($nizZaZbir['zbirBezKeceva'] >11 && $nizZaZbir['brojKeceva']==1)
        {
             $vrednostKeca=1;
        }
        else 
        {
            $vrednostKeca=0;
        }
        return $vrednostKeca;
    }
   
    public function Zbir()
    {
        $nizZaZbir=$this->ZbirBezKeca();
        return $this->VrednostKeceva()+ $nizZaZbir['zbirBezKeceva'];
    }
   //prva dva parametra objekti spil i dealer -> vidi klasu Spil i klasu Computer (dealer je uvek komputer);
    public function Hit ($spil,$dealer,$sviIgraci=null)
    {
       // $spil=&$spil;
       $zaNovu=array_values($spil);
       $novaKarta= array_shift($zaNovu);
       reset($spil);

       $key = key($spil);
       unset($spil[$key]);
       //var_dump($this->podeljeneKarte);
       $this->podeljeneKarte[]=$novaKarta;
       $dealer->recreateSpil($spil); # vidi klasu Computer
       $this->lastStep="Hit";
    }
    public function Stand($param=null,$dealer,$sviIgraci=null)
    {
        $this->lastStep="Stand";
    }
    //Double je moguc za svakog igraca samo na zbiru izmedju 9 i 11;
    // Igracu se udvostrucuje ulog (kao i svim protivnicima) i dodeljuje jedna karta 
    // nakon cega vise nema prava da uzima karte.
    // Zbog toga je Double ->lastStep;
   public function DoubleIt ($spil,$dealer,$sviIgraci)
   {
       if ($this->Zbir() >=9 && $this->Zbir() <=11)
       {
            $this->balans-=$this->ulog;//
            $this->ulog*=2;
            foreach($sviIgraci as $si)
            {
                if($si->id!=$this->id)
                {
                    $si->balans-=$si->ulog;//
                    $si->ulog*=2;
                }
            }   
            $this->Hit($spil,$dealer);
            $this->lastStep="Double";
       }    
   }         
}
