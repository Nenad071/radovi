<?php


class Karta {
   
    public $vrednost;
    public $znak;
    public $stih;
    public static $nizZaSpil= array
    (
        "pik"=>array (1,2,3,4,5,6,7,8,9,10,12,13,14),
        "karo"=>array (1,2,3,4,5,6,7,8,9,10,12,13,14),
        "herc"=>array (1,2,3,4,5,6,7,8,9,10,12,13,14),
        "tref"=> array (1,2,3,4,5,6,7,8,9,10,12,13,14)
    );
    public static $nizStihova=array(10,12,13,14);

  public static function PrepareSpil()
  {
      $nizKarata=array();
      foreach (self::$nizZaSpil as $k=>$v)
      {
          foreach ($v as $v)
          {
            $kartaInstanca = new self();
            $kartaInstanca->vrednost=$v;
            $kartaInstanca->znak=$k;
            $kartaInstanca->stih=(in_array($v, self::$nizStihova))? true:false;
            $nizKarata[]=$kartaInstanca;
          }
      }
      return $nizKarata;
  }  
}
