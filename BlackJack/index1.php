<?php
session_start();

include "iDealer.php";
include "Karta.php";
include "Spil.php";
include "Igrac.php";
include "Human.php";
include "Computer.php";
include "Kalkulacija.php";

$pobednik=null;
if(isset($_SESSION['diler']))
{
    $diler_sesija= unserialize($_SESSION['diler']);
    $kompjuteri_sesija= unserialize($_SESSION['kompjuteri']);
    $human_sesija= unserialize($_SESSION['human']);
    $ulog_sesija=$_SESSION['ulog'];
    //$balans_sesija=$_SESSION['balans'];
}

    $ulog_promenljiva= isset($_SESSION['diler']) ? $ulog_sesija: $_POST['ulog'];
//$balans_promenljiva= isset($_SESSION['diler']) ? $balans_sesija:$_POST['balans'];
if(!isset($_SESSION['diler']))
{
    $nizKarata=Karta::PrepareSpil();
    $spil=Spil::getSpilovi($_POST['spilovi'],$nizKarata);
}

$dealer=(isset($_SESSION['diler']))?$diler_sesija:Computer::getDiler($_POST['balans'],$_POST['ulog']);
if(!isset($_SESSION['diler']))
{
    $mesaniSpilovi=$dealer->MesajSpilove($spil);
}
//var_dump($mesaniSpilovi->karte);
$kompjuteri=(isset($_SESSION['diler']))?$kompjuteri_sesija:Computer::getComputeri($_POST['kompjuteri'],$_POST['balans'],$_POST['ulog']);

$human=(isset($_SESSION['diler']))?$human_sesija:Human::getHuman($_POST['ime'],$_POST['prezime'],$_POST['balans'],$_POST['ulog']);
//var_dump($_SESSION);
if (isset($_GET['nastavi_igru']))
{
    $dealer->RecreateUlog($ulog_sesija,$human,$kompjuteri);
    $dealer->IsprazniPodeljeneKarte($human,$kompjuteri);
    //var_dump($dealer);
}
if(!isset($_SESSION['diler']) || isset($_GET['nastavi_igru']))
{
$dealer->Deli($kompjuteri,$human);
//var_dump($kompjuteri);
}
if(isset($_GET['potez']))
{
    
    $pobednik=$dealer->Play($_GET['potez'],$human,$kompjuteri);
    //var_dump($pobednik);
    
}
$_SESSION['diler']=serialize($dealer);
$_SESSION['kompjuteri']=serialize($kompjuteri);
$_SESSION['human']=serialize($human);
$_SESSION['ulog']=$ulog_promenljiva;
//$_SESSION['balans']=$balans_promenljiva;

include "View.php";

