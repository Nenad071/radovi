<?php

//vidi interface -> iDealer;
class Computer extends Igrac implements iDealer
{
    public static $DealerObject;
    public static $imenaComputera = array
    (
        "Jonny","Mark","Peter","Flame","Goxy","Mileena","Djoka","Lemmy","Ozzy","Kirtana"
    );
    public static $prezimenaComputera = array
    (
        "Johnson","Peters","Myers","Chubby","Float","Dump","Throat","Perry","Deep","Stevens"
    );
    public static $akcije = array("Hit","DoubleIt","Stand");
    
    // Metodom dodeljujemo dilera koji ce deliti karte; 
    //Diler se mora znati pre nego sto pocne partija. Parametre u metodi dobija spolja preko forme;
    //Ime i prezime se dodeljuje iz gornjih nizova - random.
    public static function getDiler($balans,$ulog)
    {     
        $diler=new self();
        $diler->ime=self::$imenaComputera[rand(0,9)];
        $diler->prezime=self::$prezimenaComputera[rand(0,9)];
        $diler->is_dealer=true;// polje moze da se definise u hodu
        $diler->balans=$balans;
        $diler->ulog=$ulog;
        $diler->id=1;
        self::$DealerObject=$diler;
        return self::$DealerObject;
    }
    // Metoda vraca u nizu i dilera napravljenog u prethodnom koraku
    public static function getComputeri ($brojKompjutera,$balans,$ulog)
    {
        $nizKompjutera=[];
        $id_i=2;
        for ($i=0; $i<$brojKompjutera-1;$i++)
           {
               $igrac= new self();
               $igrac->ime=self::$imenaComputera[rand(0,9)];
               $igrac->prezime=self::$prezimenaComputera[rand(0,9)];
               $igrac->is_dealer=false;
               $igrac->balans=$balans;
               $igrac->ulog=$ulog;
               $igrac->id=$id_i;
               $nizKompjutera[]=$igrac;
               $id_i++;
           } 
       
        //nizu Kompjutera se dodeljuje i dealer
        $nizKompjutera[]=self::$DealerObject;
        return $nizKompjutera;
    }

    public function MesajSpilove($spilovi)
    {
       $nizSpojenihKarata=array();
       foreach($spilovi as $spil)
       {
            //shuffle($spil->karte);
            foreach ($spil->karte as $karta)
            {
                $nizSpojenihKarata[]=$karta;
            }
       }
       shuffle($nizSpojenihKarata);
       $promesaniSpilovi=new Spil();
       $promesaniSpilovi->karte= $nizSpojenihKarata;
       $this->promesaniSpilovi=$promesaniSpilovi;
       return $promesaniSpilovi;
    }
    //Svaki igrac dobija po dve karte osim dealera, koji dobija jednu u prvom krugu deljenja.
    public function Deli($nizKompjutera,$igrac)
    {
        $igrac->podeljeneKarte[]=$this->promesaniSpilovi->karte[0];
        $igrac->podeljeneKarte[]=$this->promesaniSpilovi->karte[1];
        unset($this->promesaniSpilovi->karte[0]);
        unset($this->promesaniSpilovi->karte[1]);
        //var_dump($this->promesaniSpilovi->karte);
        //$igrac->recreateKarte($igrac->podeljeneKarte);
        $i=2;
        foreach ($nizKompjutera as $nk)
        {
            $nk->podeljeneKarte[]=$this->promesaniSpilovi->karte[$i];
             unset($this->promesaniSpilovi->karte[$i]);
            if (!$nk->is_dealer)
            {
                $nk->podeljeneKarte[]=$this->promesaniSpilovi->karte[$i+1];
                unset($this->promesaniSpilovi->karte[$i+1]);
            }
           $i+=2;
          // $nk->recreateKarte($igrac->podeljeneKarte);
        }
    }
    
    // metod je hit/stand/double, igrac je Human;
    // Kompjuteri moraju da stanu ako im je zbir karata veci od 17.
    //
    public function Play($metod,$igrac,$nizKompjutera)
    {
        $sviIgraci=array();
        $sviIgraci=$nizKompjutera;
        $sviIgraci[]=$igrac;
        $param=null;
        if ($metod=='Hit' || $metod=="DoubleIt")
        {
            $param=$this->promesaniSpilovi->karte;    
        }
        $igrac->$metod($param,$this,$sviIgraci);
        if ($igrac->lastStep=="Stand" || $igrac->lastStep=="Double" || $igrac->Zbir()>21)
        {
            foreach($nizKompjutera as $nk)
            {
                // ova petlja je mehanizam da natera kompjutera na  "hit" ukoliko mu je zbir karata <=17 
                for($i=0;$i<20;$i++)
                {
                    if($nk->Zbir()>=9 && $nk->Zbir()<=11)
                    {
                        $akcija=self::$akcije[rand(0,1)];
                        $nk->$akcija($this->promesaniSpilovi->karte,$this,$sviIgraci);
                    }
                    if($nk->Zbir()<9 || ($nk->Zbir()>11 && $nk->Zbir()<=17))
                    {
                        $nk->Hit($this->promesaniSpilovi->karte,$this,$sviIgraci);
                    }
                    if($nk->Zbir()>17)
                    {
                        $nk->Stand(null,$this,$sviIgraci);
                    }
                    if($nk->lastStep=="Stand" || $nk->lastStep=="Double")
                        break; 
                }
            }
            // vidi klasu Kalkulacija
           return Kalkulacija::IzracunajRuku($igrac,$nizKompjutera);
        }
        // var_dump($this->promesaniSpilovi->karte);
        
    }
    public function recreateSpil($spil)
    {
        $this->promesaniSpilovi->karte=$spil;
    }
    
    public function IsprazniPodeljeneKarte ($igrac,$nizKompjutera)
    {

        $igrac->podeljeneKarte=[];
        
        foreach ($nizKompjutera as $nk)
        {
            $nk->podeljeneKarte=[];
        }
        $this->promesaniSpilovi->karte=array_values($this->promesaniSpilovi->karte);
        
    }
    // Stavljaju se ulozi svih igraca u ravnotezu kako bi svi u krugu ulozili isti novac. 
    public function RecreateUlog ($ulog,$igrac,$nizKompjutera)
    {
        $nizIgraca=array();
        $nizKompjutera[]=$igrac;
        $nizIgraca=$nizKompjutera;
       // $oduzmi=false;
        foreach ($nizIgraca as $k)
        {
            $k->lastStep="Hit";
            $k->balans=$k->balans-$ulog;
            $k->ulog=$ulog;
        }
        
//        foreach ($nizIgraca as $ni)
//        {
//            if ($ni->Zbir()<=21)
//            {
//                $oduzmi=true;
//                break;
//            }
//        }
//        $oduzmiDouble=false;
//         foreach ($nizIgraca as $i)
//            {
//                if($i->ulog==200)
//                {
//                    $oduzmiDouble=true;
//                    break;
//                }
//            }
//        if ($oduzmi) // || (!$oduzmi && $oduzmiDouble)
//        {
//            //$igrac->ulog=$ulog;
//            //$igrac->balans=$igrac->balans-$ulog;
//            foreach ($nizIgraca as $k)
//            {
//                $k->balans=$k->balans-$ulog;
//                $k->ulog=$ulog;
//            }
//        }
        
    }
}
