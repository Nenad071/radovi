<?php

class ProveraSifre
{
    public $sifra;
    public $PorukeZaFalse=[];
    public function __construct($sifra)
    {
        $this->sifra=$sifra;
    }
    public function GetSifra()
    {
        return $this->sifra;
    }
    
    public static function Factory ($sifra)
    {
        $sifra = new self($sifra);
        return $sifra;
        
    }
    
    public function ProveriSifru()
    {
        if (strlen($this->GetSifra())>8)
        {
            $sifraNiz= str_split($this->GetSifra());
            
            $upper='';
            $lower='';
            $number='';
            
            foreach ($sifraNiz as $n)
            {
                if (ctype_lower($n)) $lower=$n;
                if(ctype_upper($n)) $upper=$n;
                if (is_numeric($n))  $number=$n;
            }
            
            if ($upper!='' && $lower !='' && $number!='')
            {
                $this->PorukeZaFalse[]="Vasa sifra je dovoljno jaka!";
                return true;
            }
            else 
            {
                if ($upper=='')
                {
                    $this->PorukeZaFalse[]="Morate imati bar jedno veliko slovo";
                }
                 if ($lower=='')
                {
                    $this->PorukeZaFalse[]="Morate imati bar jedno malo slovo";
                }
                
                 if ($number=='')
                {
                    $this->PorukeZaFalse[]="Morate imati bar jedan broj";
                }
                return false;
            }
            
        }
            $this->PorukeZaFalse[]="Vasa sifra mora sadrzati najmanje 8 karaktera";
            return false;
    }
    
    public function VratiPoruke()
    {
        return $this->PorukeZaFalse;
    }
}

if (isset($_POST['submit']))
{
    
    echo $_POST['sifra']."<br>";
    $s= ProveraSifre::Factory($_POST['sifra']);
    $s->ProveriSifru();

    foreach ($s->VratiPoruke() as $poruka)
    {
        echo $poruka."<br>";
    }
}
?>
<br><a href='formaZaSifru.php'>forma</a>