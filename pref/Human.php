<?php


class Human extends Igrac {
    
   // 4 parametra koja dobijamo iz forme (index.php) kada korisnik unese podatke
    public static function getHuman($ime,$prezime,$bule)
    {
        //instanciramo objekat tipa Human
        $human=new self();
        $human->ime=$ime;
        $human->prezime=$prezime;
        $human->bule=$bule;
        $human->id=9999;
        $human->desniIgrac='desni';
        return $human;
    }
}