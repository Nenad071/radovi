<?php


 class Igrac {
 
   public $ime;
   public $prezime=null;
   public $bule=0;
   public $supeLevo=0;
   public $supeDesno=0;
   public $is_dealer=false;
   public $podeljeneKarte=[];
   public $id;
    //samo na pocetku partije, pre prvog deljenja. 
   //Nakon toga, diler je svaki put desni igrac od dilera;
   public static function GetDealer($nizKompjutera,$human)
   {
       $igraci=array();
       $igraci=$nizKompjutera;
       $igraci[]=$human;
       $dealer=$igraci[rand(0,2)];
       $dealer->is_dealer=true;
       return $dealer;
   }
   // nakon sto se dealer dodeli igracu; dealer mesa karte;
   public function MesajSpil($spil)
    {
       //$nizSpojenihKarata=array();
      
       shuffle($spil->karte);
       $promesaniSpil=new Spil();
       $promesaniSpil->karte= $spil->karte;
       var_dump($promesaniSpil->karte);
       $this->promesaniSpil=$promesaniSpil;
       var_dump($promesaniSpil);
       return $promesaniSpil;
       
   }
   
   public function DeliKompjuterima($igrac)
   {
        for($i=0;$i<5;$i++)
        {
            $igrac->podeljeneKarte[]=$this->promesaniSpil->karte[$i];
            unset($this->promesaniSpil->karte[$i]); 
        }
            $this->promesaniSpil->karte=array_values($this->promesaniSpil->karte);
   }
   
   public function DodajNaTalon($talon)
   {
       //$talon= new \Classes\Talon();
       for($i=0;$i<2;$i++)
       {
           $talon->karte[]=$this->promesaniSpil->karte[$i];
           unset($this->promesaniSpil->karte[$i]);
       }
       $this->promesaniSpil->karte=array_values($this->promesaniSpil->karte);
   }
public function Deli($nizKompjutera,$igrac,$talon)
    {
        if ($this->desniIgrac=='human')
        {
            $notDealer=null;
            $dealer=null;
            foreach ($nizKompjutera as $nk)
            {
                if (!$nk->is_dealer)
                {
                    $notDealer=$nk;
                }
                else 
                {
                    $dealer=$nk;
                }
            }
             $this->DeliKompjuterima($igrac);
             $this->DeliKompjuterima($notDealer);
             $this->DeliKompjuterima($dealer);
             $this->DodajNaTalon($talon);
             $this->DeliKompjuterima($igrac);
             $this->DeliKompjuterima($notDealer);
             $this->DeliKompjuterima($dealer);         
        }
        else 
        {
            if ($igrac->is_dealer)
            {
                $desniIgrac=null;
                $leviIgrac=null;
                foreach ($nizKompjutera as $nk)
                {
                    if ($nk->desni==true)
                    {
                        $desniIgrac=$nk;
                    }
                    else
                    {
                        $leviIgrac=$nk;
                    }
                }
                
             $this->DeliKompjuterima($desniIgrac);
             $this->DeliKompjuterima($leviIgrac);
             $this->DeliKompjuterima($igrac);
             $this->DodajNaTalon($talon);
             $this->DeliKompjuterima($desniIgrac);
             $this->DeliKompjuterima($leviIgrac);
             $this->DeliKompjuterima($igrac);    
            }
            else
            {
                $desniIgrac=null;
                $dealer=null;
                foreach ($nizKompjutera as $nk)
                {
                    if ($nk->desni==true)
                    {
                        $dealer=$nk;
                    }
                    else
                    {
                        $desniIgrac=$nk;
                    }
                }
                
                $this->DeliKompjuterima($desniIgrac);
                $this->DeliKompjuterima($igrac);
                $this->DeliKompjuterima($dealer);
                $this->DodajNaTalon($talon);
                $this->DeliKompjuterima($desniIgrac);
                $this->DeliKompjuterima($igrac);
                $this->DeliKompjuterima($dealer);
                
            }
        }
    }
}
