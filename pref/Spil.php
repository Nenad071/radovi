<?php


class Spil {
    
    //niz objekata tipa Karta
  public $karte=array();
  
  //uzimamo niz objekata tipa karta iz Karta::PrepareSpil (staticka met) i stavljamo ga kao parametar
   public static function getSpil($nizKarata)
   {
       //instanciramo objekat tipa Spil
       $spilInstanca=new self();
       $spilInstanca->karte=$nizKarata;
       return $spilInstanca;
   }
   //uzimamo broj spilova (unetih od korisnika) i niz objekata tipa karta.
   public static function getSpilovi($brojSpilova,$nizKarata)
   {
       $nizSpilova=array();
       for ($i=0; $i<$brojSpilova;$i++)
       {
           //instanciramo niz objekata tipa Spil
           $nizSpilova[]=self::getSpil($nizKarata);
       }
       //vracamo no tipa Spil
       return $nizSpilova;
   } 
}
